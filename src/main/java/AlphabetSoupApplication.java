package main.java;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class AlphabetSoupApplication {
    public static int numOfRows;
    public static int numOfCols;
    public static ArrayList<String> searchTerms = new ArrayList<>();
    public static List<GridPosition> grid = new ArrayList<>();

    public static void main(String[] args) {

        try{
            // Scan input file for Word Search
            FileInputStream sampleFile = new FileInputStream("src/main/resources/sample.txt");
            Scanner scanner = new Scanner(sampleFile);

            // Gets number of rows and col from first line of input text.
            String gridSizeRow = scanner.nextLine();
            numOfRows = Integer.parseInt(gridSizeRow.substring(0, gridSizeRow.indexOf('x')));
            numOfCols = Integer.parseInt(gridSizeRow.substring(gridSizeRow.indexOf('x') + 1));

            // Sets Search Grid from following lines in text file
            for (int i = 0; i < numOfRows; i++) {
                // Removes excess spaces for each line
                String gridRow = scanner.nextLine().replaceAll("\\s", "");

                // Sets GridPosition object for each character in the line. This is the grid that will be used to
                // search from within the application.
                for(int j = 0; j < numOfCols; j++) {
                    GridPosition characterPosition = new GridPosition();
                    characterPosition.setCharacter(gridRow.charAt(j));
                    characterPosition.setxPosition(j);
                    characterPosition.setyPosition(i);
                    grid.add(characterPosition);
                }
            }

            // Sets array of terms to find in the search grid
            while(scanner.hasNext()) {
                searchTerms.add(scanner.nextLine());
            }

            // Closes text file. The objects have been set for application, so no longer need text file.
            scanner.close();

            // Loop through all the searchTerms
            for(String word : searchTerms) {
                searchForWord(word);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Function to search for a word within an Array of GridPosition Objects.
     */
    private static void searchForWord(String word) {
        // Finds all instances with first letter withing Grid
        List<GridPosition> firstLetterList = grid.stream().filter(position -> position.getCharacter() == word.charAt(0)).collect(Collectors.toList());

        for(GridPosition firstLetter : firstLetterList) {
            for(int i = 0; i < word.length(); i++) {
                int finalI = i;

                // Searchs for next letter value & surround the previous letter: left, right, top, bottom.. etc
                GridPosition nextLetter = grid.stream().filter(adjLetter ->
                                (adjLetter.getCharacter() == word.charAt(finalI)) &&
                                        (
                                                (adjLetter.getxPosition() == firstLetter.getxPosition() - finalI && adjLetter.getyPosition() == firstLetter.getyPosition()) || // Left
                                                        (adjLetter.getxPosition() == firstLetter.getxPosition() + finalI && adjLetter.getyPosition() == firstLetter.getyPosition()) || // Right
                                                        (adjLetter.getyPosition() == firstLetter.getyPosition() + finalI && adjLetter.getxPosition() == firstLetter.getxPosition()) || // Top
                                                        (adjLetter.getyPosition() == firstLetter.getyPosition() - finalI && adjLetter.getxPosition() == firstLetter.getxPosition()) || // Bottom
                                                        (adjLetter.getxPosition() == firstLetter.getxPosition() + finalI && adjLetter.getyPosition() == firstLetter.getyPosition() + finalI) || // Top Right
                                                        (adjLetter.getxPosition() == firstLetter.getxPosition() + finalI && adjLetter.getyPosition() == firstLetter.getyPosition() - finalI) || // Bottom Right
                                                        (adjLetter.getxPosition() == firstLetter.getxPosition() - finalI && adjLetter.getyPosition() == firstLetter.getyPosition() + finalI) || // Top Left
                                                        (adjLetter.getxPosition() == firstLetter.getxPosition() - finalI && adjLetter.getyPosition() == firstLetter.getyPosition() - finalI) // Bottom Left
                                        ))
                        .findAny().orElse(null);

                // If next letter is found, sets next letter for loop. Otherwise - breaks the loop.
                if (nextLetter != null) {

                    // when the loops has reached the word length, print out the word found with position coordinates.
                    if (finalI == word.length() - 1) {
                        System.out.println(word + " "
                                + firstLetter.getyPosition() + ":" + firstLetter.getxPosition() + " "
                                + nextLetter.getyPosition() + ":" + nextLetter.getxPosition()
                        );
                    }
                } else {
                    break;
                }
            }
        }
    }
}
